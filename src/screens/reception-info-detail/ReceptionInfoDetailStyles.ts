import { Platform, StyleSheet } from "react-native";
import { colors } from "../../styles/color";
import {
  LabelLargeBold,
  LabelLargeRegular,
  HeadingXSmallBold,
  HeadingXxSmallBold,
  LabelSmallBold,
  LabelXLargeBold,
  LabelXLargeRegular,
  LinkLarge,
} from "../../styles/typography";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";

export const styles = StyleSheet.create({
  mainContainer: {
    width: "100%",
    height: "100%",
    backgroundColor: colors.selectionColor,
  },
  overallContainer: {
    flex: 1,
    width: wp("100%"),
    backgroundColor: colors.selectionColor,
  },
  topbodyContainerOne: {
    width: wp("100%"),
    height: "auto",
    paddingHorizontal: wp("2%"),
    paddingVertical: hp("2.6%"),
    gap: 32,
    backgroundColor: colors.secondary,
  },
  topbodyContainer: {
    width: wp("100%"),
    height: "auto",
    paddingHorizontal: wp("2%"),
    paddingTop: hp("2.6%"),
    gap: 32,
    backgroundColor: colors.secondary,
  },
  topinnerContainerCover: {
    width: wp("96%"),
    height: "auto",
    gap: 24,
  },
  topinnerContainer: {
    width: wp("96%"),
    height: hp("7.6%"),
    gap: 16,
    flexDirection: "row",
  },
  bigIconContainer: {
    width: 64,
    height: 64,
    borderRadius: 1000,
    gap: 8,
    backgroundColor: colors.primary,
  },
  bigIcon: {
    width: 37,
    height: 35,
    top: 14,
    left: 12,
    ...Platform.select({
      web: {
        width: 33.38,
        height: 35,
        top: 12,
      },
    }),
    color: colors.secondary,
  },
  topInnerRightInfo: {
    width: wp("89.3%"),
    height: hp("7.6%"),
    gap: hp("0.85%"),
  },
  badgeLabel: {
    width: 81,
    height: 26,
    paddingVertical: hp("0.6%"),
    ...Platform.select({
      web: {
        width: wp("6.35%"),
        paddingVertical: hp("0.45%"),
        paddingHorizontal: wp("0.65%"),
      },
    }),
    gap: hp("0.85%"),
    borderRadius: 4,
    backgroundColor: colors.bodyBackgroundColor,
  },
  badgeLabelOne: {
    width: 76,
    height: 26,
    paddingVertical: hp("0.6%"),
    ...Platform.select({
      web: {
        width: wp("5.35%"),
        paddingVertical: hp("0.45%"),
        paddingHorizontal: wp("0.65%"),
      },
    }),
    gap: hp("0.85%"),
    borderRadius: 4,
    backgroundColor: colors.neuralLightBlueColor,
  },
  badgeLabelRepresentative: {
    width: 76,
    height: 26,
    paddingVertical: hp("0.6%"),
    ...Platform.select({
      web: {
        width: wp("5.35%"),
        paddingVertical: hp("0.45%"),
        paddingHorizontal: wp("0.65%"),
      },
    }),
    gap: hp("0.85%"),
    borderRadius: 4,
    backgroundColor: colors.greenLight,
  },
  badgeLabelText: {
    fontSize: LabelSmallBold.size,
    lineHeight: LabelSmallBold.lineHeight,
    color: colors.greyTextColor,
    minWidth: wp("4%"),
    height: hp("1.95%"),
    textAlign: "center",
  },
  nameInfo: {
    width: wp("89.3%"),
    height: hp("3.9%"),
  },
  nameInfoText: {
    fontSize: HeadingXSmallBold.size,
    lineHeight: HeadingXSmallBold.lineHeight,
    color: colors.textColor,
  },
  topLine: {
    borderColor: colors.gray,
    width: wp("96%"),
    borderWidth: 1,
    transform: [{ rotate: "0deg" }],
    alignItems: "center",
  },
  bottominnerContainer: {
    width: wp("96%"),
    height: "auto",
    gap: hp("0.85%"),
  },
  row: {
    flexDirection: "row",
    width: wp("96%"),
    height: hp("2.6%"),
    gap: hp("0.85%"),
  },
  first: {
    width: wp("10.05%"),
    ...Platform.select({
      android: { width: wp("10.9%") },
      ios: { width: wp("10.9%") },
    }),
    height: hp("2.6%"),
  },
  firstText: {
    fontSize: LabelLargeBold.size,
    lineHeight: LabelLargeBold.lineHeight,
    color: colors.greyTextColor,
  },
  second: {
    width: wp("81.25%"),
    height: hp("2.6%"),
  },
  secondAddress: {
    width: wp("81.25%"),
    height: hp("6.1%"),
    gap: hp("0.85%"),
  },
  secondText: {
    fontSize: LabelLargeRegular.size,
    lineHeight: LabelLargeRegular.lineHeight,
    color: colors.textColor,
  },
  rowLink: {
    flexDirection: "row",
    width: wp("96%"),
    height: hp("2.95%"),
    gap: hp("0.85%"),
  },
  secondLink: {
    width: "auto",
    height: hp("2.95%"),
  },
  secondLinkText: {
    fontSize: LinkLarge.size,
    lineHeight: LinkLarge.lineHeight,
    color: colors.textColor,
  },
  linkText: {
    color: colors.primary,
    textDecorationLine: "underline",
  },
  separateTab: {
    width: wp("96%"),
    height: hp("4.65%"),
    gap: hp("1.75%"),
    flexDirection: "row",
  },
  tabActive: {
    width: wp("7.35%"),
    height: hp("4.65%"),
    ...Platform.select({
      android: {
        width: "auto",
      },
      ios: {
        width: "auto",
      },
    }),
    gap: hp("0.85%"),
    paddingHorizontal: wp("0.65%"),
    paddingVertical: hp("0.85%"),
    borderBottomColor: colors.primary,
    borderBottomWidth: 2,
  },
  tab: {
    width: wp("7.35%"),
    height: hp("4.65%"),
    ...Platform.select({
      android: {
        width: "auto",
      },
      ios: {
        width: "auto",
      },
    }),
    gap: hp("0.85%"),
    paddingHorizontal: wp("0.65%"),
    paddingVertical: hp("0.85%"),
  },
  tabTextActive: {
    width: wp("6.05%"),
    height: hp("2.95%"),
    ...Platform.select({
      android: {
        width: "auto",
      },
      ios: {
        width: "auto",
      },
    }),
    fontSize: LabelXLargeBold.size,
    lineHeight: LabelXLargeBold.lineHeight,
    color: colors.textColor,
    textAlign: "center",
  },
  tabText: {
    width: wp("6.05%"),
    ...Platform.select({
      android: {
        width: "auto",
      },
      ios: {
        width: "auto",
      },
    }),

    height: hp("2.95%"),

    fontSize: LabelXLargeRegular.size,
    lineHeight: LabelXLargeRegular.lineHeight,
    color: colors.greyTextColor,
    textAlign: "center",
  },
  bodyContainer: {
    width: wp("100%"),
    paddingHorizontal: wp("2%"),
    paddingTop: hp("2.62%"),
    paddingBottom: hp("13%"),
    gap: hp("2.6%"),

    backgroundColor: colors.selectionColor,
  },
  innerBodyContainer: {
    width: wp("96%"),
    borderRadius: 8,
    gap: hp("1.75%"),
    paddingHorizontal: wp("2%"),
    paddingVertical: hp("2.6%"),
    backgroundColor: colors.secondary,
  },
  innerTitleContainer: {
    width: wp("91.95%"),
    flexDirection: "row",
    justifyContent: "space-between",
  },
  innerTitle: {
    gap: hp("0.85%"),
  },
  innerTitleTextContainer: {
    width: wp("82.6%"),
    height: hp("3.25%"),
  },
  innerTitleText: {
    color: colors.textColor,
    fontSize: HeadingXxSmallBold.size,
    lineHeight: HeadingXxSmallBold.lineHeight,
  },
  btnGroup: {
    width: 96,
    height: 44,
    gap: 8,
    justifyContent: "space-between",
    flexDirection: "row",
  },
  actionBtn: {
    width: 44,
    height: 44,
    gap: hp("0.85%"),
    paddingHorizontal: wp("1.17%"),
    paddingVertical: hp("1.5%"),
    borderRadius: 4,
    borderWidth: 1,
    borderColor: colors.gray,
    backgroundColor: colors.secondary,
  },
  actionIcon: {
    color: colors.textColor,
    width: 24,
    height: 24,
  },
  line: {
    borderColor: colors.gray,
    width: wp("91.95%"),
    borderWidth: 1,
    transform: [{ rotate: "0deg" }],
    alignItems: "center",
  },
  personalRow: {
    flexDirection: "row",
    width: wp("91.95%"),
    height: hp("2.6%"),
    gap: hp("0.85%"),
    borderBottomWidth: 1,
    borderBottomColor: "transparent",
  },
  personalRowAddress: {
    flexDirection: "row",
    width: wp("91.95%"),
    height: hp("6.1%"),
    gap: hp("0.85%"),
    borderBottomWidth: 1,
    borderBottomColor: "transparent",
  },
});

export default styles;
