import React, { useState } from "react";
import {
  View,
  Pressable,
  StatusBar,
  SafeAreaView,
  ScrollView,
} from "react-native";
import { HiraginoKakuText } from "../../components/StyledText";
import { Entypo } from "@expo/vector-icons";
import styles from "./ReceptionInfoDetailStyles";
import { colors } from "../../styles/color";
import { CustomButton } from "../../components/basics/Button";
import { Header } from "../../components/basics/header";
import { MaterialIcons } from "@expo/vector-icons";
import { Ionicons } from "@expo/vector-icons";
import { NavigationProp, useRoute } from "@react-navigation/native";
import { ReceptionInfoDeleteDialog } from "../reception-info-delete-dialog/ReceptionInfoDeleteDialog";

type Props = {
  navigation: NavigationProp<any, any>;
};
export const ReceptionInfoDetail = ({ navigation }: Props) => {
  const route = useRoute();
  const { userId, eventId, receptionId, pgType } = route.params as {
    userId: string;
    eventId: number;
    receptionId: number;
    pgType: string;
  };

  const [selectedLayout, setSelectedLayout] = useState("individual");
  const [isDeleteModalVisible, setIsDeleteModalVisible] = useState(false);

  const handleReceptionInfoEdit = () => {
    navigation.navigate("ReceptionInfoEdit", {
      userId: 1,
      eventId: 1,
      receptionId: 1,
      registerType: "individual",
      pgType: pgType,
    });
  };
  const handleReceptionInfoEditGroup = () => {
    navigation.navigate("ReceptionInfoEdit", {
      userId: 1,
      eventId: 1,
      receptionId: 1,
      registerType: "group",
      pgType: pgType,
    });
  };
  const handleBacktoEventDetail = () => {
    navigation.navigate("EventDetail", {
      userId: userId,
      eventId: 1,
      pgType: pgType,
    });
  };

  /// delete Dialog
  var [selectedRegisterType, setSelectedRegisterType] = useState("");
  var [selectedRole, setSelectedRole] = useState("");

  const handleReceptionInfoDelete = () => {
    setIsDeleteModalVisible(true);
    setSelectedRegisterType("individual");
  };
  const handleReceptionInfoGroupDelete = (rolename: string) => {
    setIsDeleteModalVisible(true);
    setSelectedRegisterType("group");
    setSelectedRole(rolename);
  };
  const handleDeleteCancelButton = () => {
    setIsDeleteModalVisible(false);
    navigation.navigate("ReceptionInfoDetail", {
      userId,
      eventId,
      receptionId,
      pgType,
    });
  };
  const handleDeleteButton = () => {
    setIsDeleteModalVisible(false);
    if (selectedRegisterType == "group" && selectedRole == "一緒に受付") {
      navigation.navigate("ReceptionInfoDetail", {
        userId,
        eventId,
        receptionId,
        pgType,
      });
    } else {
      navigation.navigate("EventDetail", {
        userId,
        eventId,
        receptionId,
        pgType,
      });
    }
  };

  var showOnlyInPerson = false;
  const renderLayout = () => {
    if (selectedLayout === "individual") {
      return (
        <View style={styles.bodyContainer}>
          <View style={styles.innerBodyContainer}>
            <View style={styles.innerTitleContainer}>
              <View style={styles.innerTitleTextContainer}>
                <View style={showOnlyInPerson ? styles.innerTitle : null}>
                  <HiraginoKakuText style={styles.innerTitleText}>
                    個人情報
                  </HiraginoKakuText>
                </View>
              </View>

              <View style={styles.btnGroup}>
                <CustomButton
                  text=""
                  style={styles.actionBtn}
                  type="ButtonMGray"
                  icon={
                    <MaterialIcons
                      name="mode-edit"
                      size={24}
                      color="black"
                      style={styles.actionIcon}
                    />
                  }
                  iconPosition="center"
                  onPress={handleReceptionInfoEdit}
                ></CustomButton>
                <CustomButton
                  text=""
                  style={styles.actionBtn}
                  type="ButtonMGray"
                  icon={
                    <MaterialIcons
                      name="delete"
                      size={24}
                      color="black"
                      style={styles.actionIcon}
                    />
                  }
                  iconPosition="center"
                  onPress={handleReceptionInfoDelete}
                ></CustomButton>
              </View>
            </View>
            <View style={styles.line}></View>
            <View style={styles.personalRow}>
              <View style={styles.first}>
                <HiraginoKakuText style={styles.firstText}>
                  お名前
                </HiraginoKakuText>
              </View>
              <View style={styles.second}>
                {showOnlyInPerson ? (
                  <HiraginoKakuText style={styles.secondText} normal>
                    散布 瑠音
                  </HiraginoKakuText>
                ) : (
                  <HiraginoKakuText style={styles.secondText} normal>
                    出茂 ふみ
                  </HiraginoKakuText>
                )}
              </View>
            </View>
            <View style={styles.personalRow}>
              <View style={styles.first}>
                <HiraginoKakuText style={styles.firstText}>
                  お名前（カナ）
                </HiraginoKakuText>
              </View>
              <View style={styles.second}>
                {showOnlyInPerson ? (
                  <HiraginoKakuText style={styles.secondText} normal>
                    サンプ ルネ
                  </HiraginoKakuText>
                ) : (
                  <HiraginoKakuText style={styles.secondText} normal>
                    イズモ フミ
                  </HiraginoKakuText>
                )}
              </View>
            </View>
            <View style={styles.personalRow}>
              <View style={styles.first}>
                <HiraginoKakuText style={styles.firstText}>
                  生年月日
                </HiraginoKakuText>
              </View>
              <View style={styles.second}>
                {showOnlyInPerson ? (
                  <HiraginoKakuText style={styles.secondText} normal>
                    2012/04/02
                  </HiraginoKakuText>
                ) : (
                  <HiraginoKakuText style={styles.secondText} normal>
                    1986/02/21
                  </HiraginoKakuText>
                )}
              </View>
            </View>
            <View style={styles.personalRow}>
              <View style={styles.first}>
                <HiraginoKakuText style={styles.firstText}>
                  性別
                </HiraginoKakuText>
              </View>
              <View style={styles.second}>
                <HiraginoKakuText style={styles.secondText} normal>
                  女性
                </HiraginoKakuText>
              </View>
            </View>
            <View style={styles.personalRowAddress}>
              <View style={styles.first}>
                <HiraginoKakuText style={styles.firstText}>
                  住所
                </HiraginoKakuText>
              </View>
              <View style={styles.secondAddress}>
                <HiraginoKakuText style={styles.secondText} normal>
                  515-0004
                </HiraginoKakuText>
                <HiraginoKakuText style={styles.secondText} normal>
                  三重県松阪市なんとか町11-2asa
                  マンション名102あああああああああああああああああ
                </HiraginoKakuText>
              </View>
            </View>
            {!showOnlyInPerson ? (
              <View style={styles.personalRow}>
                <View style={styles.first}>
                  <HiraginoKakuText style={styles.firstText}>
                    代表者との関係
                  </HiraginoKakuText>
                </View>
                <View style={styles.second}>
                  <HiraginoKakuText style={styles.secondText} normal>
                    配偶者
                  </HiraginoKakuText>
                </View>
              </View>
            ) : null}
          </View>
        </View>
      );
    } else {
      return (
        <View style={styles.bodyContainer}>
          {!showOnlyInPerson ? (
            <View style={styles.innerBodyContainer}>
              <View style={styles.innerTitleContainer}>
                <View style={styles.innerTitle}>
                  <View style={[styles.badgeLabelRepresentative]}>
                    <HiraginoKakuText style={styles.badgeLabelText}>
                      受付代表
                    </HiraginoKakuText>
                  </View>
                  <View style={styles.innerTitleTextContainer}>
                    <HiraginoKakuText style={styles.innerTitleText}>
                      個人情報
                    </HiraginoKakuText>
                  </View>
                </View>

                <View style={styles.btnGroup}>
                  <CustomButton
                    text=""
                    style={styles.actionBtn}
                    type="ButtonMGray"
                    icon={
                      <MaterialIcons
                        name="mode-edit"
                        size={24}
                        color="black"
                        style={styles.actionIcon}
                      />
                    }
                    iconPosition="center"
                    onPress={handleReceptionInfoEditGroup}
                  ></CustomButton>
                  <CustomButton
                    text=""
                    style={styles.actionBtn}
                    type="ButtonMGray"
                    icon={
                      <MaterialIcons
                        name="delete"
                        size={24}
                        color="black"
                        style={styles.actionIcon}
                      />
                    }
                    iconPosition="center"
                    onPress={() => handleReceptionInfoGroupDelete("受付代表者")}
                  ></CustomButton>
                </View>
              </View>
              <View style={styles.line} />
              <View style={styles.personalRow}>
                <View style={styles.first}>
                  <HiraginoKakuText style={styles.firstText}>
                    お名前
                  </HiraginoKakuText>
                </View>
                <View style={styles.second}>
                  {showOnlyInPerson ? (
                    <HiraginoKakuText style={styles.secondText} normal>
                      散布 瑠音
                    </HiraginoKakuText>
                  ) : (
                    <HiraginoKakuText style={styles.secondText} normal>
                      出茂 進次郎
                    </HiraginoKakuText>
                  )}
                </View>
              </View>
              <View style={styles.personalRow}>
                <View style={styles.first}>
                  <HiraginoKakuText style={styles.firstText}>
                    お名前（カナ）
                  </HiraginoKakuText>
                </View>
                <View style={styles.second}>
                  <HiraginoKakuText style={styles.secondText} normal>
                    イズモ シンジロウ
                  </HiraginoKakuText>
                </View>
              </View>
              <View style={styles.personalRow}>
                <View style={styles.first}>
                  <HiraginoKakuText style={styles.firstText}>
                    生年月日
                  </HiraginoKakuText>
                </View>
                <View style={styles.second}>
                  <HiraginoKakuText style={styles.secondText} normal>
                    1985/11/02
                  </HiraginoKakuText>
                </View>
              </View>
              <View style={styles.personalRow}>
                <View style={styles.first}>
                  <HiraginoKakuText style={styles.firstText}>
                    性別
                  </HiraginoKakuText>
                </View>
                <View style={styles.second}>
                  <HiraginoKakuText style={styles.secondText} normal>
                    男性
                  </HiraginoKakuText>
                </View>
              </View>
              <View style={styles.personalRowAddress}>
                <View style={styles.first}>
                  <HiraginoKakuText style={styles.firstText}>
                    住所
                  </HiraginoKakuText>
                </View>
                <View style={styles.secondAddress}>
                  <HiraginoKakuText style={[styles.secondText]} normal>
                    515-0004
                  </HiraginoKakuText>
                  <HiraginoKakuText style={[styles.secondText]} normal>
                    三重県松阪市なんとか町11-2
                    マンション名102あああああああああああああああああ
                  </HiraginoKakuText>
                </View>
              </View>
              {!showOnlyInPerson ? (
                <View style={styles.personalRow}>
                  <View style={styles.first}>
                    <HiraginoKakuText style={styles.firstText}>
                      代表者との関係
                    </HiraginoKakuText>
                  </View>
                  <View style={styles.second}>
                    <HiraginoKakuText style={styles.secondText} normal>
                      本人
                    </HiraginoKakuText>
                  </View>
                </View>
              ) : null}

              <View style={styles.line} />

              <View style={styles.personalRow}>
                <View style={styles.first}>
                  <HiraginoKakuText style={styles.firstText}>
                    本人確認
                  </HiraginoKakuText>
                </View>
                <View style={styles.second}>
                  <HiraginoKakuText style={styles.secondText} normal>
                    済み
                  </HiraginoKakuText>
                </View>
              </View>
              <View style={styles.personalRow}>
                <View style={styles.first}>
                  <HiraginoKakuText style={styles.firstText}>
                    LGaP_ID
                  </HiraginoKakuText>
                </View>
                <View style={styles.second}>
                  <HiraginoKakuText style={styles.secondText} normal>
                    12345678ABCDEFG
                  </HiraginoKakuText>
                </View>
              </View>
            </View>
          ) : null}
          <View style={styles.innerBodyContainer}>
            <View style={styles.innerTitleContainer}>
              <View style={styles.innerTitle}>
                <View style={[styles.badgeLabel]}>
                  <HiraginoKakuText style={styles.badgeLabelText}>
                    一緒に受付
                  </HiraginoKakuText>
                </View>
                <View>
                  <View style={styles.innerTitleTextContainer}>
                    <HiraginoKakuText style={styles.innerTitleText}>
                      個人情報
                    </HiraginoKakuText>
                  </View>
                </View>
              </View>

              <View style={styles.btnGroup}>
                <CustomButton
                  text=""
                  style={styles.actionBtn}
                  type="ButtonMGray"
                  icon={
                    <MaterialIcons
                      name="mode-edit"
                      size={24}
                      color="black"
                      style={styles.actionIcon}
                    />
                  }
                  iconPosition="center"
                  onPress={handleReceptionInfoEditGroup}
                ></CustomButton>
                <CustomButton
                  text=""
                  style={styles.actionBtn}
                  type="ButtonMGray"
                  icon={
                    <MaterialIcons
                      name="delete"
                      size={24}
                      color="black"
                      style={styles.actionIcon}
                    />
                  }
                  iconPosition="center"
                  onPress={() => handleReceptionInfoGroupDelete("一緒に受付")}
                ></CustomButton>
              </View>
            </View>
            <View style={styles.line} />
            <View style={styles.personalRow}>
              <View style={styles.first}>
                <HiraginoKakuText style={styles.firstText}>
                  お名前
                </HiraginoKakuText>
              </View>
              <View style={styles.second}>
                <HiraginoKakuText style={styles.secondText} normal>
                  出茂 太郎
                </HiraginoKakuText>
              </View>
            </View>
            <View style={styles.personalRow}>
              <View style={styles.first}>
                <HiraginoKakuText style={styles.firstText}>
                  お名前（カナ）
                </HiraginoKakuText>
              </View>
              <View style={styles.second}>
                <HiraginoKakuText style={styles.secondText} normal>
                  イズモ タロウ
                </HiraginoKakuText>
              </View>
            </View>
            <View style={styles.personalRow}>
              <View style={styles.first}>
                <HiraginoKakuText style={styles.firstText}>
                  生年月日
                </HiraginoKakuText>
              </View>
              <View style={styles.second}>
                <HiraginoKakuText style={styles.secondText} normal>
                  2000/01/02
                </HiraginoKakuText>
              </View>
            </View>
            <View style={styles.personalRow}>
              <View style={styles.first}>
                <HiraginoKakuText style={styles.firstText}>
                  性別
                </HiraginoKakuText>
              </View>
              <View style={styles.second}>
                <HiraginoKakuText style={styles.secondText} normal>
                  未入力
                </HiraginoKakuText>
              </View>
            </View>
            <View style={styles.personalRowAddress}>
              <View style={styles.first}>
                <HiraginoKakuText style={styles.firstText}>
                  住所
                </HiraginoKakuText>
              </View>
              <View style={styles.secondAddress}>
                <HiraginoKakuText style={styles.secondText} normal>
                  515-0004
                </HiraginoKakuText>
                <HiraginoKakuText style={styles.secondText} normal>
                  三重県松阪市なんとか町11-2
                  マンション名102あああああああああああああああああ
                </HiraginoKakuText>
              </View>
            </View>
            {!showOnlyInPerson ? (
              <View style={styles.personalRow}>
                <View style={styles.first}>
                  <HiraginoKakuText style={styles.firstText}>
                    代表者との関係
                  </HiraginoKakuText>
                </View>
                <View style={styles.second}>
                  <HiraginoKakuText style={styles.secondText} normal>
                    子ども
                  </HiraginoKakuText>
                </View>
              </View>
            ) : null}
          </View>
        </View>
      );
    }
  };

  var handleTabClick = (layout: string) => {
    setSelectedLayout(layout);
  };
  return (
    <SafeAreaView style={styles.mainContainer}>
      <StatusBar barStyle="dark-content" />
      <Header
        titleName="もどる"
        middleTitleName="＜人名＞"
        buttonName=""
        hasButton={false}
        icon={<Entypo name="chevron-left" size={24} color={colors.primary} />}
        iconPosition="front"
        onPressLeft={handleBacktoEventDetail}
      ></Header>
      <ScrollView>
        <View style={styles.overallContainer}>
          {showOnlyInPerson ? (
            <View style={[styles.topbodyContainerOne]}>
              <View style={styles.topinnerContainerCover}>
                <View style={styles.topinnerContainer}>
                  <View style={styles.bigIconContainer}>
                    <Ionicons
                      name="person"
                      color="white"
                      style={styles.bigIcon}
                      size={40}
                    />
                  </View>
                  <View style={styles.topInnerRightInfo}>
                    <View style={[styles.badgeLabelOne]}>
                      <HiraginoKakuText style={styles.badgeLabelText}>
                        本人のみ
                      </HiraginoKakuText>
                    </View>

                    <View style={styles.nameInfo}>
                      <HiraginoKakuText style={styles.nameInfoText}>
                        散布 瑠音
                      </HiraginoKakuText>
                    </View>
                  </View>
                </View>

                <View style={styles.topLine} />

                <View style={styles.bottominnerContainer}>
                  <View style={styles.row}>
                    <View style={styles.first}>
                      <HiraginoKakuText style={styles.firstText}>
                        イベント名
                      </HiraginoKakuText>
                    </View>
                    <View style={styles.second}>
                      <HiraginoKakuText style={styles.secondText} normal>
                        出茂市マラソン 2023
                      </HiraginoKakuText>
                    </View>
                  </View>

                  <View style={styles.row}>
                    <View style={styles.first}>
                      <HiraginoKakuText style={styles.firstText}>
                        受付日
                      </HiraginoKakuText>
                    </View>
                    <View style={styles.second}>
                      <HiraginoKakuText style={styles.secondText} normal>
                        2023/05/12 14:00
                      </HiraginoKakuText>
                    </View>
                  </View>

                  <View style={styles.row}>
                    <View style={styles.first}>
                      <HiraginoKakuText style={styles.firstText}>
                        受付方法
                      </HiraginoKakuText>
                    </View>
                    <View style={styles.second}>
                      <HiraginoKakuText style={styles.secondText} normal>
                        手入力
                      </HiraginoKakuText>
                    </View>
                  </View>
                </View>
              </View>
            </View>
          ) : (
            <View style={[styles.topbodyContainer]}>
              <View style={styles.topinnerContainerCover}>
                <View style={styles.topinnerContainer}>
                  <View style={styles.bigIconContainer}>
                    <Ionicons
                      name="person"
                      color="white"
                      style={styles.bigIcon}
                      size={40}
                    />
                  </View>
                  <View style={styles.topInnerRightInfo}>
                    {showOnlyInPerson ? (
                      <View style={[styles.badgeLabelOne]}>
                        <HiraginoKakuText style={styles.badgeLabelText}>
                          本人のみ
                        </HiraginoKakuText>
                      </View>
                    ) : (
                      <View style={[styles.badgeLabel]}>
                        <HiraginoKakuText style={styles.badgeLabelText}>
                          一緒に受付
                        </HiraginoKakuText>
                      </View>
                    )}

                    <View style={styles.nameInfo}>
                      {showOnlyInPerson ? (
                        <HiraginoKakuText style={styles.nameInfoText}>
                          散布 瑠音
                        </HiraginoKakuText>
                      ) : (
                        <HiraginoKakuText style={styles.nameInfoText}>
                          出茂 ふみ
                        </HiraginoKakuText>
                      )}
                    </View>
                  </View>
                </View>

                <View style={styles.topLine} />

                <View style={styles.bottominnerContainer}>
                  <View style={styles.row}>
                    <View style={styles.first}>
                      <HiraginoKakuText style={styles.firstText}>
                        イベント名
                      </HiraginoKakuText>
                    </View>
                    <View style={styles.second}>
                      <HiraginoKakuText style={styles.secondText} normal>
                        出茂市マラソン 2023
                      </HiraginoKakuText>
                    </View>
                  </View>

                  <View style={styles.row}>
                    <View style={styles.first}>
                      <HiraginoKakuText style={styles.firstText}>
                        受付日
                      </HiraginoKakuText>
                    </View>
                    <View style={styles.second}>
                      <HiraginoKakuText style={styles.secondText} normal>
                        2023/05/12 14:00
                      </HiraginoKakuText>
                    </View>
                  </View>

                  <View style={styles.row}>
                    <View style={styles.first}>
                      <HiraginoKakuText style={styles.firstText}>
                        受付方法
                      </HiraginoKakuText>
                    </View>
                    <View style={styles.second}>
                      <HiraginoKakuText style={styles.secondText} normal>
                        アプリ (自己QR) 同時受付
                      </HiraginoKakuText>
                    </View>
                  </View>

                  <View style={styles.rowLink}>
                    <View style={styles.first}>
                      <HiraginoKakuText style={styles.firstText}>
                        受付代表者
                      </HiraginoKakuText>
                    </View>
                    <View style={styles.secondLink}>
                      <HiraginoKakuText
                        style={[styles.secondLinkText, styles.linkText]}
                        normal
                      >
                        出茂 進次郎
                      </HiraginoKakuText>
                    </View>
                  </View>
                </View>
              </View>
              {!showOnlyInPerson ? (
                <View style={styles.separateTab}>
                  <Pressable onPress={() => handleTabClick("individual")}>
                    {selectedLayout === "individual" ? (
                      <View style={styles.tabActive}>
                        <HiraginoKakuText style={styles.tabTextActive}>
                          受付情報
                        </HiraginoKakuText>
                      </View>
                    ) : (
                      <View style={styles.tab}>
                        <HiraginoKakuText style={styles.tabText} normal>
                          受付情報
                        </HiraginoKakuText>
                      </View>
                    )}
                  </Pressable>

                  <Pressable onPress={() => handleTabClick("group")}>
                    {selectedLayout === "group" ? (
                      <View style={styles.tabActive}>
                        <HiraginoKakuText style={styles.tabTextActive}>
                          同時受付
                        </HiraginoKakuText>
                      </View>
                    ) : (
                      <View style={styles.tab}>
                        <HiraginoKakuText style={styles.tabText} normal>
                          同時受付
                        </HiraginoKakuText>
                      </View>
                    )}
                  </Pressable>
                </View>
              ) : null}
            </View>
          )}
          {renderLayout()}
        </View>
        {isDeleteModalVisible && (
          <ReceptionInfoDeleteDialog
            onCancelButtonPress={handleDeleteCancelButton}
            onDeleteButtonPress={handleDeleteButton}
            registerType={selectedRegisterType}
            roleName={selectedRole}
          />
        )}
      </ScrollView>
    </SafeAreaView>
  );
};
