import React, { useState } from "react";
import { View } from "react-native";
import ModalComponent from "../../components/basics/ModalComponent";
type LogoutProps = {
  onCancelButtonPress?: () => void;
  onLogoutButtonPress?: () => void;
};
export const Logout = (props: LogoutProps) => {
  const [isModalVisible, setModalVisible] = useState(true);

  return (
    <View>
      {isModalVisible && (
        <ModalComponent
          text="ログアウトしますか？"
          firstButtonText="キャンセル"
          secondButtonText="ログアウト"
          onFirstButtonPress={props.onCancelButtonPress}
          onSecondButtonPress={props.onLogoutButtonPress}
          toggleModal={props.onCancelButtonPress}
          firstButtonType="ButtonMGray"
          secondButtonType="ButtonMPrimary"
          closeButtonVisible={false}
        ></ModalComponent>
      )}
    </View>
  );
};
