import React, { useState } from "react";
import {
  View,
  Text,
  TouchableOpacity,
  SafeAreaView,
  StatusBar,
  TextInput,
  ScrollView,
  Pressable,
  KeyboardAvoidingView,
  Platform,
} from "react-native";
import styles from "./EventEditStyles";
import { MaterialIcons, Entypo } from "@expo/vector-icons";
import { HiraginoKakuText } from "../../components/StyledText";
import { Header } from "../../components/basics/header";
import { colors } from "../../styles/color";
import { CustomButton } from "../../components/basics/Button";
import { NavigationProp, useRoute } from "@react-navigation/native";
import { EventSaveDialog } from "../event-save-dialog/EventSaveDialog";

type Props = {
  navigation: NavigationProp<any, any>;
};
export const EventEdit = ({ navigation }: Props) => {
  const route = useRoute();
  const { userId, eventId } = route.params as {
    userId: string;
    eventId: number;
  };
  const [inputBoxes, setInputBoxes] = useState([
    { value: "会場A" },
    { value: "会場B" },
  ]);
  const [selectedButton, setSelectedButton] = useState<string | null>(
    "preparation"
  );

  const addInputBox = () => {
    setInputBoxes([...inputBoxes, { value: "" }]);
  };

  const removeInputBox = (removedIndex: number) => {
    setInputBoxes(inputBoxes.filter((box, index) => index !== removedIndex));
  };

  const handleButtonPress = (buttonName: string) => {
    if (buttonName === selectedButton) {
      if (selectedButton === "create") {
        navigation.navigate("EventList", {
          userId: userId,
        });
      }

      setSelectedButton(null);
    } else {
      setSelectedButton(buttonName);
    }
  };
  const handleButtonPressMain = () => {
    if (selectedButton == "create") {
      navigation.navigate("EventList", { userId: userId, eventId: 0 });
    }
    if (selectedButton == "preparation") {
      setIsSaveModalVisible(true);
    } else if (selectedButton == "acceptance") {
      setIsSaveModalVisible(true);
    } else if (selectedButton == "close") {
      setIsSaveModalVisible(true);
    } else if (selectedButton == "archive") {
      setIsSaveModalVisible(true);
    }
  };

  // Event Save Dialog
  const [isSaveModalVisible, setIsSaveModalVisible] = useState(false);
  const handleCloseEventEdit = () => {
    setIsSaveModalVisible(true);
  };
  const handleCancelButton = () => {
    setIsSaveModalVisible(false);
    navigation.navigate("EventEdit", {
      userId: userId,
      eventId: eventId,
    });
  };
  const handleNotSaveButton = () => {
    setIsSaveModalVisible(false);
    navigation.navigate("EventList", {
      userId: userId,
    });
  };
  const handleSaveButton = () => {
    setIsSaveModalVisible(false);
    navigation.navigate("EventList", {
      userId: userId,
      eventId: eventId,
    });
  };
  return (
    <KeyboardAvoidingView
      behavior={Platform.OS === "ios" ? "padding" : "height"}
    >
      <SafeAreaView style={styles.mainContainer}>
        <StatusBar barStyle="dark-content" />
        <Header middleTitleName="イベント編集" buttonName="">
          <CustomButton
            text="閉じる"
            type="ButtonSDefault"
            icon={<Entypo name="cross" size={24} color={colors.primary} />}
            iconPosition="front"
            onPress={handleCloseEventEdit}
          />
        </Header>
        <ScrollView style={styles.scrollViewContent}>
          <View style={styles.container}>
            <View style={styles.bodyContainer}>
              <View style={styles.eventTitleContainer}>
                <View style={styles.eventHeadingLblContainer}>
                  <HiraginoKakuText
                    style={[styles.bodyText, styles.LabelLargeBold]}
                  >
                    イベント名
                  </HiraginoKakuText>
                  <View style={styles.statusLabelContainer}>
                    <View
                      style={[styles.statusBox, styles.eventTitleStatusBox]}
                    >
                      <HiraginoKakuText
                        style={[
                          styles.statusLabelText,
                          styles.eventRedStatusLblText,
                        ]}
                      >
                        必須
                      </HiraginoKakuText>
                    </View>
                  </View>
                </View>
                <View>
                  <TextInput
                    style={styles.eventInputBox}
                    defaultValue="出茂マラソン大会2024"
                    editable={false}
                  />
                </View>
              </View>
              <View style={styles.eventTimeContainer}>
                <View style={styles.eventTimeLblGpContainer}>
                  <View style={styles.eventHeadingLblContainer}>
                    <HiraginoKakuText
                      style={[styles.bodyText, styles.LabelLargeBold]}
                    >
                      イベント期間
                    </HiraginoKakuText>
                    <View style={styles.statusLabelContainer}>
                      <View style={styles.statusBox}>
                        <HiraginoKakuText style={styles.statusLabelText}>
                          任意
                        </HiraginoKakuText>
                      </View>
                    </View>
                  </View>
                  <HiraginoKakuText normal style={styles.eventLabel}>
                    開始日と終了日を入力してください
                  </HiraginoKakuText>
                </View>
                <View style={styles.dateTimeSelectContainer}>
                  <View style={styles.DateContainer}>
                    <HiraginoKakuText
                      style={[styles.bodyText, styles.LabelLargeBold]}
                    >
                      開始日
                    </HiraginoKakuText>
                    <View style={styles.datePickerBox}>
                      <HiraginoKakuText
                        normal
                        style={[styles.bodyText, styles.LabelLargeBold]}
                      >
                        2024/05/01
                      </HiraginoKakuText>
                      <Pressable style={styles.calendarIconContainer}>
                        <MaterialIcons
                          name="calendar-today"
                          size={22}
                          color={colors.activeCarouselColor}
                          style={styles.calendarIcon}
                        />
                      </Pressable>
                    </View>
                  </View>
                  <View style={styles.waveDash}>
                    <Text style={styles.LabelLargeBold}> ~ </Text>
                  </View>
                  <View style={styles.DateContainer}>
                    <HiraginoKakuText
                      style={[styles.bodyText, styles.LabelLargeBold]}
                    >
                      終了日
                    </HiraginoKakuText>
                    <View style={styles.datePickerBox}>
                      <HiraginoKakuText
                        normal
                        style={[styles.bodyText, styles.LabelLargeBold]}
                      >
                        2024/05/01
                      </HiraginoKakuText>
                      <Pressable style={styles.calendarIconContainer}>
                        <MaterialIcons
                          name="calendar-today"
                          size={22}
                          color={colors.activeCarouselColor}
                          style={styles.calendarIcon}
                        />
                      </Pressable>
                    </View>
                  </View>
                </View>
              </View>
              <View style={styles.eventVenueContainer}>
                <View style={styles.eventHeadingLblContainer}>
                  <HiraginoKakuText
                    style={[styles.bodyText, styles.LabelLargeBold]}
                  >
                    会場
                  </HiraginoKakuText>
                  <View style={styles.statusLabelContainer}>
                    <View style={styles.statusBox}>
                      <HiraginoKakuText style={styles.statusLabelText}>
                        任意
                      </HiraginoKakuText>
                    </View>
                  </View>
                </View>
                <View style={styles.venueInputBoxesContainer}>
                  {inputBoxes.map((box, index) => (
                    <View key={index} style={styles.venueInputBox}>
                      <TextInput
                        style={[styles.eventInputBox, styles.venueInput]}
                        defaultValue={box.value}
                        editable={false}
                      />
                      {inputBoxes.length > 1 && (
                        <TouchableOpacity
                          onPress={() => removeInputBox(index)}
                          style={styles.venueCrossIconContainer}
                        >
                          <MaterialIcons
                            name="close"
                            size={22}
                            color="#515867"
                          />
                        </TouchableOpacity>
                      )}
                    </View>
                  ))}
                  <TouchableOpacity
                    style={styles.venueBtnContainer}
                    onPress={addInputBox}
                  >
                    <MaterialIcons name="add" size={22} color="#515867" />
                    <HiraginoKakuText
                      style={[
                        styles.bodyText,
                        styles.LabelLargeBold,
                        styles.venueBtnText,
                      ]}
                    >
                      会場を追加
                    </HiraginoKakuText>
                  </TouchableOpacity>
                </View>
              </View>
              <View style={styles.horizontalLine} />
              <View style={styles.eventReceptionContainer}>
                <View style={styles.eventHeadingLblContainer}>
                  <HiraginoKakuText
                    style={[styles.bodyText, styles.LabelLargeBold]}
                  >
                    イベントの受付状況
                  </HiraginoKakuText>
                  <View style={styles.statusLabelContainer}>
                    <View
                      style={[styles.statusBox, styles.eventTitleStatusBox]}
                    >
                      <HiraginoKakuText
                        style={[
                          styles.statusLabelText,
                          styles.eventRedStatusLblText,
                        ]}
                      >
                        必須
                      </HiraginoKakuText>
                    </View>
                  </View>
                </View>
                <HiraginoKakuText normal style={styles.eventLabel}>
                  受付可能にすると受付アプリに表示されます
                </HiraginoKakuText>
                <View style={styles.eventReceptionBtnContainer}>
                  <TouchableOpacity
                    style={[
                      styles.receptionBtn,
                      selectedButton === "preparation" && styles.btnSelected,
                    ]}
                    onPress={() => handleButtonPress("preparation")}
                  >
                    <HiraginoKakuText
                      style={[styles.bodyText, styles.LabelLargeBold]}
                    >
                      準備中
                    </HiraginoKakuText>
                  </TouchableOpacity>
                  <TouchableOpacity
                    style={[
                      styles.receptionBtn,
                      styles.eventMedianBtn,
                      selectedButton === "acceptance" && styles.btnSelected,
                    ]}
                    onPress={() => handleButtonPress("acceptance")}
                  >
                    <HiraginoKakuText
                      style={[styles.bodyText, styles.LabelLargeBold]}
                    >
                      受付可能
                    </HiraginoKakuText>
                  </TouchableOpacity>
                  <TouchableOpacity
                    style={[
                      styles.receptionBtn,
                      styles.eventMedianBtn,
                      selectedButton === "close" && styles.btnSelected,
                    ]}
                    onPress={() => handleButtonPress("close")}
                  >
                    <HiraginoKakuText
                      style={[styles.bodyText, styles.LabelLargeBold]}
                    >
                      受付終了
                    </HiraginoKakuText>
                  </TouchableOpacity>
                  <TouchableOpacity
                    style={[
                      styles.receptionBtn,
                      styles.eventLargeBtn,
                      selectedButton === "archive" && styles.btnSelected,
                    ]}
                    onPress={() => handleButtonPress("archive")}
                  >
                    <HiraginoKakuText
                      style={[styles.bodyText, styles.LabelLargeBold]}
                    >
                      アーカイブ
                    </HiraginoKakuText>
                  </TouchableOpacity>
                </View>
              </View>
            </View>
            <View style={styles.createBtnContainer}>
              <TouchableOpacity
                style={[
                  styles.createBtn,
                  selectedButton === "create" && styles.btnSelected,
                ]}
                onPress={() => handleButtonPressMain()}
              >
                <HiraginoKakuText
                  style={[styles.createBtnText, styles.LabelLargeBold]}
                >
                  保存する
                </HiraginoKakuText>
              </TouchableOpacity>
            </View>
          </View>
        </ScrollView>
        {isSaveModalVisible && (
          <EventSaveDialog
            onUnsaveButtonPress={handleNotSaveButton}
            onSaveButtonPress={handleSaveButton}
            onCancelButtonPress={handleCancelButton}
          />
        )}
      </SafeAreaView>
    </KeyboardAvoidingView>
  );
};
